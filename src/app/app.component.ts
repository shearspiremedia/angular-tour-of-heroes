import { Component } from '@angular/core';

// Decorators are functions that modify JavaScript classes
// They attach metadata to a class.
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // in TypeScript some type declarations are optional. String is implied here.
  title = 'Tour of Heroes';
}

/*
Every Angular application has at least one component, the root component (this one) that
connects a component hierarchy with the page document object model (DOM).
Each component defines a class that contains application data and logic,
and is associated with an HTML template that defines a view to be displayed in a target environment.
The @Component() decorator identifies the class immediately below it as a component,
and provides the template and related component-specific metadata.
*/

