// The three imports below this comment are created by default in a new app
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
// The next set of imports are for angular libraries
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// This next one required: npm install angular-in-memory-web-api --save
// See the comment in the imports array for more details
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
// The InMemoryDataService provides a json object that is similar to what
// might be received by a call to a remote api
import { InMemoryDataService } from './in-memory-data.service';
// The AppRoutingModule holds the navigation paths to display individual components on demand
import { AppRoutingModule } from './app-routing.module';
// The rest of the imports are for components created just for this app
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    DashboardComponent,
    HeroSearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
